package com.mmall.common;

import java.util.Set;

import com.google.common.collect.Sets;

/**
 * Created by qujian on 2017/6/3.
 */
public class Const {

    public static final String CURRENT_USER = "CURRENT_USER";

    public static final String EMAIL        = "email";

    public static final String USERNAME     = "username";

    public interface Cart{
        int CHECKD = 1; //购物车选中状态
        int UN_CHECKD = 0;  //购物车中未选中状态
        String LIMIT_NUM_FAIL = "LIMIT_NUM_FAIL";
        String LIMIT_NUM_SUCCESS = "LIMIT_NUM_SUCCESS";
    }

    public interface ProductListOrderBy {
        Set<String> PRICE_ASD_DESC = Sets.newHashSet("price_desc", "price_asc");
    }

    public interface Role {
        int ROLE_CUSTOMER = 0; //普通用户
        int ROLE_ADMIN    = 1; //管理员
    }

    public enum ProductStatusEnum {
        ON_SALE(1, "在售"),
        OFF_SHELF(2,"下架"),
        DELETE(3,"删除");



        private int    code;

        private String value;

        ProductStatusEnum(int code, String value) {
            this.code = code;
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }

    public enum OrderStatusEnum{
        CANCELED(0,"已取消"),
        NO_PAY(10,"未支付"),
        PAID(20,"已支付"),
        SHIPPED(40,"已发货"),
        ORDER_SUCCESS(50,"订单完成"),
        ORDER_CLOSE(60,"订单关闭");


        private int code;
        private String value;

        OrderStatusEnum(int code, String value) {
            this.code = code;
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public int getCode() {
            return code;
        }


        /**
         * 根据code获取value
         * @param code
         * @return
         */
        public static OrderStatusEnum codeOf(int code){
            for(OrderStatusEnum orderStatusEnum : values()){
                if(orderStatusEnum.getCode() == code){
                    return orderStatusEnum;
                }
            }
            throw  new RuntimeException("没有该枚举类型");
        }
    }

    public interface AlipayCallback{
        String WAIT_BUYER_PAY = "WAIT_BUYER_PAY";
        String TRADE_STATUS_TRADE_SUCCESS = "TRADE_SUCCESS";

        String RESPONSE_SUCCESS = "success";
        String RESPONSE_FAILED = "failed";
    }

    public enum PayPlatFormEnum{

        ALIPAY(1,"支付宝");

        private int code;
        private String value;


        PayPlatFormEnum(int code, String value) {
            this.code = code;
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public int getCode() {
            return code;
        }
    }

    public enum PaymentTypeEnum{

        ONLINE_PAY(1,"在线支付");

        private int code;
        private String value;


        PaymentTypeEnum(int code, String value) {
            this.code = code;
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public int getCode() {
            return code;
        }

        /**
         * 根据code获取value
         * @param code
         * @return
         */
        public static PaymentTypeEnum codeOf(int code){
            for(PaymentTypeEnum paymentTypeEnum : values()){
                if(paymentTypeEnum.getCode() == code){
                    return paymentTypeEnum;
                }
            }
            throw  new RuntimeException("没有该枚举类型");
        }
    }
}
