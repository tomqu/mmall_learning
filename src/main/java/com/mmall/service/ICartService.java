package com.mmall.service;

import com.mmall.common.ServerResponse;
import com.mmall.pojo.vo.CartVo;

/**
 * Created by qujian on 2017/6/29.
 */
public interface ICartService {

    ServerResponse<CartVo> add(Integer userId, Integer productId, Integer count);

    ServerResponse<CartVo> update(Integer userId, Integer productId, Integer count);

    ServerResponse<CartVo> deleteProduct(Integer userId, String productIds);

    ServerResponse<CartVo> list(Integer userId);

    ServerResponse<CartVo> selectOrUnselect(Integer userId,Integer productId,Integer isChecked);

    ServerResponse<Integer> getCartProductCount(Integer userId);
}
