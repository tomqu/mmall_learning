package com.mmall.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by qujian on 2017/6/21.
 */
public interface IFileService {

    String upload(MultipartFile file, String path);

}
