package com.mmall.service;

import com.github.pagehelper.PageInfo;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.Cart;

import java.util.List;
import java.util.Map;

/**
 * Created by qujian on 2017/7/14.
 */
public interface IOrderService {

    //protal

    ServerResponse pay(Integer userId, Long orderNo, String path);

    ServerResponse aliCallback(Map<String,String> params);

    ServerResponse<Boolean> queryOrderPayStatus(Integer userId,Long orderNo);

    ServerResponse createOrder(Integer userId, Integer shippingId);

    ServerResponse<String> cancelOrder(Integer userId, Long orderNo);

    ServerResponse<List<Cart>> getOrderCartProduct(Integer userId);

    ServerResponse getOrderdetail(Integer userId, Long orderNo);

    ServerResponse<PageInfo> getOrderList(Integer userId, int pageNum, int pageSize);

    //backend

    ServerResponse<PageInfo> manageList(int pageNum, int pageSize);

    ServerResponse manageDetail(Long orderNo);

    ServerResponse<PageInfo> manageSearch(Long orderNo,int pageNum, int pageSize);

    ServerResponse<String> manageSendGoods(Long orderNo);

}
