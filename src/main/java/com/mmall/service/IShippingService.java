package com.mmall.service;

import com.github.pagehelper.PageInfo;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.Shipping;

/**
 * Created by qujian on 2017/7/4.
 */
public interface IShippingService {

    ServerResponse add(Shipping shipping);

    ServerResponse del(Integer userId,Integer shippingId);

    ServerResponse update(Shipping shipping);

    ServerResponse<Shipping> selectCheck(Integer userId,Integer shippingId);

    ServerResponse<PageInfo> list(Integer pageNum, Integer pageSize, Integer userId);


}
